import React, { Component } from 'react';
import { HashRouter, Route, Link } from 'react-router-dom';

import './App.css';
import Home from './home/Home';
import Company from './company/Company';
import Companies from './company/Companies';
import CompanyDetail from './company/CompanyDetail';
import Person from './person/Person';
import Persons from './person/Persons';
import CompanyShareholder from './company/CompanyShareholder';

const Header = () => (
  <div className="Header">
    <nav className="navbar navbar-toggleable-md navbar-light bg-faded">
      <Link to="/" className="navbar-brand">Company owners</Link>
      <ul className="navbar-nav mr-auto">
        <li className="nav-item"><Link to="/" className="nav-link">Home</Link></li>
        <li className="nav-item"><Link to="/companies" className="nav-link">Companies</Link></li>
        <li className="nav-item"><Link to="/persons" className="nav-link">Persons</Link></li>
      </ul>
    </nav>
  </div>
);

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <HashRouter>
          <div>
            <Route path='*' component={ Header } />
            <Route exact path='/' component={ Home } />
            <Route path='/company/new' component={ Company } />
            <Route path='/company/:id/view' component={ CompanyDetail } />
            <Route path='/company/:id/edit' component={ Company } />
            <Route path='/companies' component={ Companies } />
            <Route path='/person/new' component={ Person } />
            <Route path='/person/:id/edit' component={ Person } />
            <Route path='/persons' component={ Persons } />
            <Route path='/company/:id/share/person' component={ CompanyShareholder } />
            <Route path='/company/:id/share/company' component={ CompanyShareholder } />
          </div>
        </HashRouter>
      </div>
    );
  }
}
