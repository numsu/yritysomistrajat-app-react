import React, { Component } from 'react';

export default class Home extends Component {
  render() {
    return (
      <div className="Home content">
        <div>
          <h2 className="pagehead">Company owners and shares</h2>
          <hr />
          <div>
            <h5>The app lets you add companies and their owners, which can be either natural or legal persons.</h5>
          </div>
        </div>
      </div>
    );
  }
}
