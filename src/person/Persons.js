import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Http from '../common/Http';

export default class Persons extends Component {
  state = {
    persons: []
  };

  componentDidMount() {
    Http.GET('/person/getall').then((res) => {
      this.setState({ persons: res });
    });
  }

  render() {
    return (
      <div className="Persons content">
      <div className="tablehead">
        <h2 className="pagehead pull-left">Persons</h2>
        <Link to="/person/new">
          <button className="btn btn-primary btn-md pull-right">
            <i className="fa fa-star-o" /> New person
          </button>
        </Link>
      </div>
        <PersonList persons={ this.state.persons } />
      </div>
    );
  }
}

class PersonList extends Component {
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Identification</th>
              <th>Land</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            { this.props.persons.map(person => <PersonListRow key={ person.id } person={ person } />) }
          </tbody>
        </table>
      </div>
    );
  }
}

class PersonListRow extends Component {
  render() {
    const { person } = this.props;
    return (
      <tr>
        <td>{ person.fullname }</td>
        <td>{ person.identification }</td>
        <td>{ person.land }</td>
        <td className="text-right">
          <Link to={ '/person/' + person.id + '/edit' }>
            <button type="button" className="btn btn-sm btn-secondary">
              <i className="fa fa-pencil"></i> Edit
            </button>
          </Link>
        </td>
      </tr>
    );
  }
}
