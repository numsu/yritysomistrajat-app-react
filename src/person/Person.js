import React, { Component } from 'react';
const moment = require('moment');

import Http from '../common/Http';
import FormInput from '../common/FormInput';

export default class Person extends Component {
  constructor(props) {
    super(props);
    this.editMode = !!props.match.params.id;
  }

  componentDidMount() {
    if (this.editMode) {
      Http.GET('/person/get/' + this.props.match.params.id).then((res) => {
        res.birthdate = moment(res.birthdate).format('DD.MM.YYYY');
        this.setState({ person: res });
      });
    } else {
      this.setState({
        person: {
          firstname: '',
          name: '',
          identification: '',
          land: '',
          birthdate: '',
          taxcode: ''
        }
      });
    }
  }

  render() {
    if (this.state && this.state.person) {
      return (
        <div className="Person content">
          <h2 className="pagehead">{ this.editMode ? 'Edit person' : 'New person' }</h2>
          <hr />
          <PersonForm person={ this.state.person } />
        </div>
      );
    } else {
      return <div />;
    }
  }
}

class PersonForm extends Component {
  constructor(props) {
    super(props);
    this.state = props;
  }

  handleDelete = () => {
    this.handleChange('delete', true);
    this.handleSubmit();
  }

  handleSubmit = () => {
    const person = this.state.person;
    person.birthdate = moment(person.birthdate, 'DD.MM.YYYY').toDate();
    Http.POST('/person/save', person).then((res) => {
      if (!res.id)
        location.hash = '/persons';
      else
        location.hash = '/person/' + res.id + '/edit';
    });
  }

  handleChange = (modelKey, e) => {
    const person = this.state.person;
    person[modelKey] = (e.target) ? e.target.value : e;
    this.setState(person);
  }

  render() {
    const { person } = this.state;
    return (
      <div className="form">
        <form noValidate>
          <FormInput  model={ person }
                      modelKey="firstname"
                      label="Firstname"
                      modelChange={ this.handleChange } />
          <FormInput  model={ person }
                      modelKey="name"
                      label="Lastname"
                      modelChange={ this.handleChange } />
          <FormInput  model={ person }
                      modelKey="identification"
                      label="Identification"
                      modelChange={ this.handleChange } />
          <FormInput  model={ person }
                      modelKey="land"
                      label="Land"
                      modelChange={ this.handleChange } />
          <FormInput  model={ person }
                      modelKey="birthdate"
                      label="Birthdate"
                      modelChange={ this.handleChange } />
          <FormInput  model={ person }
                      modelKey="taxcode"
                      label="Taxcode"
                      modelChange={ this.handleChange } />

          <div className="btn-toolbar btn-end col-md-6">
            <button className="btn btn-md btn-danger float-right" type="button" onClick={ this.handleDelete }>
              <i className="fa fa-trash-o" /> Delete
            </button>&nbsp;
            <button className="btn btn-md btn-success float-right" type="button" onClick={ this.handleSubmit }>
              <i className="fa fa-save" /> Save
            </button>
          </div>
        </form>
      </div>
    );
  }
}
