import React, { Component } from 'react';

import Http from '../common/Http';
import FormInput from '../common/FormInput';

export default class Company extends Component {
  constructor(props) {
    super(props);
    this.editMode = !!props.match.params.id;
  }

  componentDidMount() {
    if (this.editMode) {
      Http.GET('/company/get/' + this.props.match.params.id).then((res) => {
        this.setState({ company: res });
      });
    } else {
      this.setState({
        company: {
          name: '',
          identification: '',
          land: ''
        }
      });
    }
  }

  render() {
    if (this.state && this.state.company) {
      return (
        <div className="Company content">
          <h2 className="pagehead">{ this.editMode ? 'Edit company' : 'New company' }</h2>
          <hr />
          <CompanyForm company={ this.state.company } />
        </div>
      );
    } else {
      return <div />;
    }
  }
}

class CompanyForm extends Component {
  constructor(props) {
    super(props);
    this.state = props;
  }

  handleDelete = () => {
    this.handleChange('delete', true);
    this.handleSubmit();
  }

  handleSubmit = () => {
    Http.POST('/company/save', this.state.company).then((res) => {
      if (!res.id)
        location.hash = '/companies';
      else
        location.hash = '/company/' + res.id + '/edit';
    });
  }

  handleChange = (modelKey, e) => {
    const company = this.state.company;
    company[modelKey] = (e.target) ? e.target.value : e;
    this.setState(company);
  }

  render() {
    const { company } = this.state;
    return (
      <div className="form">
        <form noValidate>
          <FormInput  model={ company }
                      modelKey="name"
                      label="Name"
                      modelChange={ this.handleChange } />
          <FormInput  model={ company }
                      modelKey="identification"
                      label="Identification"
                      modelChange={ this.handleChange } />
          <FormInput  model={ company }
                      modelKey="land"
                      label="Land"
                      modelChange={ this.handleChange } />

          <div className="btn-toolbar btn-end col-md-6">
            <button className="btn btn-md btn-danger float-right" type="button" onClick={ this.handleDelete }>
              <i className="fa fa-trash-o" /> Delete
            </button>&nbsp;
            <button className="btn btn-md btn-success float-right" type="button" onClick={ this.handleSubmit }>
              <i className="fa fa-save" /> Save
            </button>
          </div>
        </form>
      </div>
    );
  }
}
