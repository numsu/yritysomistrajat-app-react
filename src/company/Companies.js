import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Http from '../common/Http';

export default class Companies extends Component {
  state = {
    companies: []
  };

  componentDidMount() {
    Http.GET('/company/getall').then((res) => {
      this.setState({ companies: res });
    });
  }

  render() {
    return (
      <div className="Companies content">
        <div className="tablehead">
          <h2 className="pagehead pull-left">Companies</h2>
          <Link to="/company/new">
            <button className="btn btn-primary btn-md pull-right">
              <i className="fa fa-star-o" /> New company
            </button>
          </Link>
        </div>
        <CompanyList companies={ this.state.companies } />
      </div>
    );
  }
}

class CompanyList extends Component {
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Identification</th>
              <th>Land</th>
              <th>Shareholders</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            { this.props.companies.map(company => <CompanyListRow key={ company.id } company={ company } />) }
          </tbody>
        </table>
      </div>
    );
  }
}

class CompanyListRow extends Component {
  render() {
    const { company } = this.props;
    return (
      <tr>
        <td>{ company.name }</td>
        <td>{ company.identification }</td>
        <td>{ company.land }</td>
        <td>{ company.shares.length || 0 }</td>
        <td className="text-right">
          <Link to={ '/company/' + company.id + '/edit' }>
            <button type="button" className="btn btn-sm btn-secondary">
              <i className="fa fa-pencil"></i> Edit
            </button>
          </Link>&nbsp;
          <Link to={ '/company/' + company.id + '/view' }>
            <button type="button" className="btn btn-sm btn-secondary">
              <i className="fa fa-search"></i> View
            </button>
          </Link>
        </td>
      </tr>
    );
  }
}
