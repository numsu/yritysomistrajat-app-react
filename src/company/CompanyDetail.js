import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Http from '../common/Http';

export default class CompanyDetail extends Component {
  state = {
    company: {
      shares: []
    }
  };

  componentDidMount() {
    Http.GET('/company/get/' + this.props.match.params.id).then((res) => {
      this.setState({ company: res });
    });
  }

  handleDelete = (deleted) => {
    deleted.delete = true;
    Http.POST('/share/save', deleted).then(() => {
      this.componentDidMount();
    });
  }

  render() {
    const { company } = this.state;
    return (
      <div className="CompanyDetail content">
        <div className="tablehead">
          <h2 className="pagehead pull-left">{ company.name }</h2>
          <div className="btn-toolbar btn-end">
            <Link to={ '/company/' + company.id + '/share/person' }>
              <button className="btn btn-primary">
                <i className="fa fa-star-o" /> New person shareholder
              </button>
            </Link>&nbsp;
            <Link to={ '/company/' + company.id + '/share/company' }>
              <button className="btn btn-primary">
                <i className="fa fa-star-o" /> New company shareholder
              </button>
            </Link>
          </div>
        </div>
        <h5 className="pagehead pull-left">Shareholders</h5>
        <ShareList shares={ company.shares } handleDelete={ this.handleDelete } />
      </div>
    );
  }
}

class ShareList extends Component {
  render() {
    const { handleDelete, shares } = this.props;
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Type</th>
              <th>Name</th>
              <th>Holding</th>
              <th>Identification</th>
              <th>Land</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            { shares.map(share => <ShareListRow key={ share.id } share={ share } handleDelete={ handleDelete } />) }
          </tbody>
        </table>
      </div>
    );
  }
}

class ShareListRow extends Component {
  render() {
    const { share, handleDelete } = this.props;
    return (
      <tr>
        <td>{ share.shareholder.type === 2 ? 'Person' : 'Company' }</td>
        <td>{ share.shareholder.name }</td>
        <td>{ share.holding }%</td>
        <td>{ share.shareholder.identification }</td>
        <td>{ share.shareholder.land }</td>
        <td className="text-right">
          <button type="button" className="btn btn-sm btn-danger" onClick={ () => handleDelete(share) }>
            <i className="fa fa-trash-o"></i> Delete
          </button>
        </td>
      </tr>
    );
  }
}
