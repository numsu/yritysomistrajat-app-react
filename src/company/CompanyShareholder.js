import React, { Component } from 'react';

import Http from '../common/Http';

const ShareholderHeader = () => {
  return (
    <div className="ShareholderHeader content">
      <div className="tablehead">
        <h2 className="pagehead pull-left">Add new shareholder</h2>
      </div>
    </div>
  );
}

export default class CompanyShareholder extends Component {
  state = {
    company: {},
    companies: []
  };

  componentDidMount() {
    Http.GET('/company/get/' + this.props.match.params.id).then((res) => {
      this.setState({ company: res });
    });
  }

  render() {
    if (location.hash.endsWith('person')) {
      return (
        <div>
          <ShareholderHeader />
          <ShareholderList company={ this.state.company } type="person" />
        </div>
      );
    } else {
      return (
        <div>
          <ShareholderHeader />
          <ShareholderList company={ this.state.company } type="company" />
        </div>
      );
    }
  }
}

class ShareholderList extends Component {
  state = {
    type: '',
    data: []
  };

  componentDidMount() {
    if (this.props.type === 'company') {
      Http.GET('/company/getall').then((res) => {
        this.setState({ type: this.props.type, data: res });
      });
    } else {
      Http.GET('/person/getall').then((res) => {
        this.setState({ type: this.props.type, data: res });
      });
    }
  }

  handleSubmit = (id, holding) => {
    Http.POST('/share/save', {
      company: { id: this.props.company.id },
      shareholder: { id: id, type: this.props.type === 'company' ? 1 : 2 },
      holding: holding
    }).then((res) => {
      location.hash = '/company/' + this.props.company.id + '/view';
    });
  }

  render() {
    if (this.state.data.length > 0) {
      return (
        <div>
          <table className="table">
            <thead>
              <tr>
                <th>Name</th>
                <th>Identification</th>
                <th>Land</th>
                <th>Holding %</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              { this.state.data.map(item => <ShareholderListRow key={ item.id } item={ item } handleSubmit={ this.handleSubmit } />) }
            </tbody>
          </table>
        </div>
      );
    } else {
      return <div />;
    }
  }
}

class ShareholderListRow extends Component {
  state = {
    model: ''
  }

  modelChange = (e) => {
    this.setState({ model: e.target.value });
  }

  render() {
    const { item, handleSubmit } = this.props;
    return (
      <tr>
        <td>{ item.fullname || item.name }</td>
        <td>{ item.identification }</td>
        <td>{ item.land }</td>
        <td>
          <input  className="form-control input-sm"
                  value={ this.state.model }
                  onChange={ (e) => { this.modelChange(e) } }
                  type="number" />
        </td>
        <td className="text-right">
          <button type="button" className="btn btn-md btn-success" onClick={ () => handleSubmit(item.id, this.state.model) }>
            <i className="fa fa-save"></i> Save share
          </button>
        </td>
      </tr>
    );
  }
}
