import { Component } from 'react';

export default class Http extends Component {
  static baseurl() {
    return location.protocol + '//' + location.hostname + ':8080';
  }

  static GET(uri) {
    return fetch(Http.baseurl() + uri).then((res) => {
        return res.json();
    });
  }

  static POST(uri, reqBody) {
    return fetch(Http.baseurl() + uri, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(reqBody)
    }).then((res) => {
      return res.json();
    });
  }
}
