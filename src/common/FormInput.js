import React, { Component } from 'react';

export default class FormInput extends Component {
  render() {
    const { model, modelKey, label, modelChange } = this.props;
    return (
      <div className="form-group row">
        <label className="col-md-2 col-form-label" htmlFor={ label }>{ label }</label>
        <div className="col-md-4">
          <input  className="form-control input-sm"
                  id={ label }
                  value={ model[modelKey] }
                  onChange={ (e) => { modelChange(modelKey, e) } }
                  required
                  type="text" />
        </div>
      </div>
    );
  }
}
